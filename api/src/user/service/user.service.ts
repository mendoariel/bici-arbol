import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable, throwError } from 'rxjs';
import { AuthService } from 'src/auth/services/auth.service';
import { Repository } from 'typeorm';
import { UserEntity } from '../models/user.entity';
import { User, UserRole } from '../models/user.interface';

import { switchMap, map, catchError } from 'rxjs/operators';

import {  
    paginate,
    Pagination,
    IPaginationOptions,
  } from 'nestjs-typeorm-paginate';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity) private readonly userRespository: Repository<UserEntity>, 
        private authService: AuthService
    ) {}

    create(user: User): Observable<User> {

        return this.authService.hashPassword(user.password).pipe(
            switchMap((passwordHash: string) => {

                user.password = passwordHash;
                user.role = UserRole.USER;

                return  from(this.userRespository.save(user)).pipe(
                    map((user: User) => {
                        const {password, ...result} = user;
                        return result;
                    }),
                    catchError(err => throwError(err))
                );
                
            })
        );
        
        
        // return  from(this.userRespository.save(user));
    }

    findOne(id: number): Observable<User> {
        return from(this.userRespository.findOne(id)).pipe(
            map((user: User) => {
                const {password, ...result} = user;
                return result;
            })
        )
    }

    findAll(): Observable<User[]> {
        return from(this.userRespository.find()).pipe(
            map((users: User[]) => {
                 users.forEach(function(v) {delete v.password});
                 return users;   
            })
        )
    }

    paginate(options: IPaginationOptions ):Observable<Pagination<User>> {
        return from(paginate<User>(this.userRespository, options)).pipe(
            map((userPageable: Pagination<User>) => {
                userPageable.items.forEach(function (v) {delete v.password})  

                return userPageable
            })
        )
    }

    deleteOne(id:number): Observable<any> {
        return from(this.userRespository.delete(id))
    }

    updateOne(id:number, user: User): Observable<any> {
        delete user.email;
        delete user.password; 
        delete user.role;

        return from(this.userRespository.update(id, user))
    }

    updateRoleOfUser(id: number, user: User): Observable<any> {
        return from(this.userRespository.update(id, user))
    }

    login(user: User): Observable<string> {
        return this.validateUser(user.email, user.password).pipe(
            switchMap((user: User) => {
                if(user) {
                    return this.authService.generateJWT(user).pipe(map((jwt: string) => jwt));
                } else return 'Wrong Credentials'; 
            })
        )
    }

    validateUser(email: string, password: string): Observable<User> {
         return this.findByMail(email).pipe(
             switchMap((user: User) => this.authService.comparePassword(password, user.password).pipe(
                 map((match: boolean) => {
                     if(match) {
                         const {password, ...result} = user;
                         return result;
                     } else throw Error;
                 })
             ))
         )
    }

    findByMail(email: string):Observable<User> {
        return from(this.userRespository.findOne({email}))
    }
}
 